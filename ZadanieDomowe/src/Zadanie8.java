import static java.lang.Math.abs;

public class Zadanie8 {
    public static void main(String[] args) {
        /*Stwórz kod w metodzie main, w którym będziemy mieć dwie zmienne przechowujące dwie
        liczby całkowite. Jeżeli iloczyn tych liczb będzie różny od zera, to program ma wypisać
        wartość bezwzględną z iloczynu tych dwóch wartości. Jeżeli iloczyn jest równy 0, to funkcja
        ma wypisać wartość 1. */
        int a = 3;
        int b = -6;
        int iloczyn = a * b;
        if (iloczyn != 0) {
            System.out.println(abs(iloczyn));
        } else if (iloczyn == 0) {
            System.out.println("1");
        }
    }
}