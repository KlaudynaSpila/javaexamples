public class Zadanie13 {
    /* Napisz kod, który będzie posiadał tablicę liczb valuesTable oraz zmienną givenNumber z
liczbą całkowitą. Metoda wypiszę liczbę takich kombinacji, że suma dwóch dowolnych
elementów z tablicy równa jest liczbie przechowywanej w zmiennej givenNumber.
Przykład: Lista zawiera elementy 1, 2, 3, 3, 6, 8
Zmienna givenNumber zawiera 9 więc zwrócona zostanie wartość 2 ponieważ są dwie
kombinacje, że suma dwóch liczb daje 9 (1 + 8, 3 + 6) */
    public static void main(String[] args) {
        int[] valuesTable = {4, 6, 7, 12, 5,8,4};
        int givenNumber = 12;
        int count = 0;

        for (int i = 0; i < valuesTable.length; i++) {
            for (int j = 0; j < valuesTable.length; j++) {
                if(i!=j){
                    var sum = valuesTable[i] + valuesTable[j];
                    if (sum == givenNumber) {
                        count = count + 1;
                    } }
            }
        }
        System.out.println("Combinations of numbers: " + count/2);
    }
}

