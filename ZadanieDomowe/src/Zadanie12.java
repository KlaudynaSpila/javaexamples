import java.util.Arrays;

public class Zadanie12 {
    /* Napisz kod, który będzie będzie posiadał dwie tablice typu int a zwracana jest tablica o
    rozmiarze tablicy większej(jeśli jedna z dwóch tablic jest większa) i zawiera sumę elementów
    na tych samych pozycjach z obydwu tablic. Przykład:
    Tablica A: {1, 4, 6, 2, 2}
    Tablica B: {2, 1, 2, 1, 0, 4, 8}
    Zwracana tablica: {3, 5, 8, 3, 2, 4, 8} */
    public static void main(String[] args) {
        int[] Tablica1 = {1, 3, 8, 3, 9, 6};
        int[] Tablica2 = {5, 8, 4, 2};
        int length = Tablica1.length < Tablica2.length ? Tablica1.length
                : Tablica2.length;
        int[] result = new int[length];


        for (int i = 0; i < length; i++) {
            result[i] = Tablica1[i] + Tablica2[i];
        }

        System.out.println( "Nowa tablica: " + Arrays.toString(result));


    }
}
