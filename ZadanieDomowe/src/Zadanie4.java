
//Zrobione
public class Zadanie4 {
    // Napisz kod, który wyliczy i wypisze sumę liczb parzystych w zakresie od 0 do n (np. dla 5 jest
    //to 2 + 4 = 6)
    public static void main(String[] args) {
        int n = 8;
        int sum =0;
        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
}